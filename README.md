**How to use:** 

`Terraform.gitlab-ci.yml` is the template and must be referenced in the main ci file with "include:".
See `Pipeline.gitlab-ci.yml` for an example.

In this example's case, if you want to destroy instead of deploy, uncomment the destroy step and comment everything else in the main `Pipeline.gitlab-ci.yml` file.  
All values and steps in the main file overwrite the template if present.